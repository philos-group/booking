import { auth } from 'firebase/app'

export default async function () {
  const fba = auth()
  const cred = await fba.getRedirectResult()
  if (!cred.user) {
    if (!fba.currentUser || (fba.currentUser && fba.currentUser.isAnonymous)) {
      const provider = new auth.GoogleAuthProvider()
      await auth().signInWithRedirect(provider)
      return
    }
  }
  const usr = cred.user || fba.currentUser
  const token = await usr.getIdTokenResult()
  if (token.claims.Group !== 'admins') {
    if (usr.metadata.creationTime === usr.metadata.lastSignInTime) {
      const provider = new auth.GoogleAuthProvider()
      await usr.reauthenticateWithRedirect(provider)
    }
    window.onNuxtReady(() => {
      window.$nuxt.error({ statusCode: 403, message: 'UnAuthorized' })
    })
  }
}

import Vue from 'vue'
import Vuelidate from 'vuelidate'
import swal from 'sweetalert2'

export default ({}, inject) => {
  inject('swal', swal)
  Vue.use(Vuelidate)
}

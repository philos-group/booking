import { firestore } from 'firebase/app'
import moment from 'moment'

export const state = () => ({
  list: [],
  filter: {
    from: moment().add(-30, 'days'),
    to: moment().add(30, 'days'),
    location: null,
  },
  behavior: {
    sortBy: 'date',
    sortDir: 'desc',
    pageIndex: 0,
    pageSize: 100,
  },
  item: null,
  unsubscribe: null,
})

export const mutations = {
  SET_FILTER(state, filter) {
    state.filter = filter
  },
  SET_LIST(state, list) {
    state.list = list
  },
  SET_ITEM(state, item) {
    state.item = item
  },
  SET_UNSUBSCRIBE(state, unsubscribe) {
    state.unsubscribe = unsubscribe
  },
}

export const actions = {
  async add({}, payload) {
    payload.date = firestore.Timestamp.fromDate(payload.date.toDate())
    payload.countLeft = payload.count
    await firestore().collection('schedule').add(payload)
  },
  async remove({}, id) {
    const fs = firestore()
    const requests = fs.collection('requests').where('schedule.id', '==', id)
    const schedule = fs.doc(`schedule/${id}`)
    await fs.runTransaction(async (tr) => {
      ;(await requests.get()).forEach((doc) => {
        tr.delete(doc.ref)
      })
      tr.delete(schedule)
    })
  },
  async filter({ commit, dispatch }, filter) {
    commit('SET_FILTER', filter)
    await dispatch('stop')
    await dispatch('listen')
  },
  listen({ commit, state }, id) {
    let unsubscribe = null
    if (id) {
      const query = _buildQueyById(id)
      unsubscribe = query.onSnapshot((snapshot) => {
        const item = _buildItemFromSnapshot(snapshot)
        commit('SET_ITEM', item)
      })
    } else {
      const query = _buildQuery(state)
      unsubscribe = query.onSnapshot((snapshot) => {
        const list = _buildListFromSnapshot(snapshot)
        commit('SET_LIST', list)
      })
    }
    commit('SET_UNSUBSCRIBE', unsubscribe)
  },
  stop({ commit, state }) {
    if (state.unsubscribe) state.unsubscribe()
    commit('SET_UNSUBSCRIBE', null)
  },
}

// ===== Helpers ======
function _buildQueyById(id) {
  let doc = firestore().doc(`schedule/${id}`)
  return doc
}

function _buildQuery(state) {
  let query = firestore().collection('schedule')
  if (state.filter.location)
    query = query.where('location', '==', state.filter.location)
  if (state.filter.from)
    query = query.where(
      'date',
      '>=',
      firestore.Timestamp.fromDate(state.filter.from.toDate())
    )
  if (state.filter.to)
    query = query.where(
      'date',
      '<=',
      firestore.Timestamp.fromDate(state.filter.to.toDate())
    )
  query = query
    .orderBy(state.behavior.sortBy, state.behavior.sortDir)
    //.startAfter() must use doc()
    .limit(state.behavior.pageSize)
  return query
}

function _buildItemFromSnapshot(snapshot) {
  const row = snapshot.data()
  return {
    id: snapshot.id,
    ...row,
    date: moment(row.date.toDate()),
    percent: (row.countLeft * 100) / row.count,
  }
}

function _buildListFromSnapshot(snapshot) {
  return snapshot.docs.map(_buildItemFromSnapshot)
}

import { auth } from 'firebase/app'

// const baseURL = 'http://localhost:5001/philos-booking/us-central1/api'
const baseURL = 'https://us-central1-philos-booking.cloudfunctions.net/api'

export const state = () => ({ deadMessages: [], isLoading: false })

export const mutations = {
  SET_LOADING(state, isLoading) {
    state.isLoading = isLoading
  },
  SET_MESSAGES(state, messages) {
    state.deadMessages = messages
  },
}

export const actions = {
  async loadDeadMessages({ commit }) {
    commit('SET_LOADING', true)
    const token = await auth().currentUser.getIdToken()

    const res = await fetch(`${baseURL}/process/dead-messages`, {
      method: 'GET',
      headers: {
        authorization: 'Bearer ' + token,
      },
    })
    const messages = await res.json()
    commit('SET_MESSAGES', messages)
    commit('SET_LOADING', false)
  },
  async retry({ commit, dispatch }) {
    window.$nuxt.$root.$loading.start()
    commit('SET_LOADING', true)
    const token = await auth().currentUser.getIdToken()
    await fetch(`${baseURL}/process/retry`, {
      method: 'PATCH',
      headers: {
        Authorization: 'Bearer ' + token,
      },
    })
    dispatch('loadDeadMessages')
    commit('SET_LOADING', false)
    window.$nuxt.$root.$loading.finish()
  },
}

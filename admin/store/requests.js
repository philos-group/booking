import { firestore } from 'firebase/app'
import moment from 'moment'

export const state = () => ({
  eventId: null,
  list: null,
  unsubscribe: null,
})

export const mutations = {
  SET_LIST(state, list) {
    state.list = list
  },
  SET_UNSUBSCRIBE(state, unsubscribe) {
    state.unsubscribe = unsubscribe
  },
}

export const actions = {
  async add({ rootState }, { event, request }) {
    const newReq = firestore().collection('requests').doc()
    await firestore().runTransaction(async (tr) => {
       tr.set(newReq, {
        ...request,
        createdBy: rootState.user.profile.displayName,
        date: firestore.Timestamp.now(),
        schedule: {
          date: firestore.Timestamp.fromDate(event.date.toDate()),
          id: event.id,
          location: event.location,
        },
      })
    })
  },
  listen({ commit }, id) {
    let unsubscribe = null
    const query = _buildQuery(id)
    unsubscribe = query.onSnapshot((snapshot) => {
      const list = _buildListFromSnapshot(snapshot)
      commit('SET_LIST', list)
    })
    commit('SET_UNSUBSCRIBE', unsubscribe)
  },
  stop({ commit, state }) {
    if (state.unsubscribe) state.unsubscribe()
    commit('SET_UNSUBSCRIBE', null)
  },
}

function _buildQuery(id) {
  let query = firestore()
    .collection('requests')
    .where('schedule.id', '==', id)
    //.orderBy('date', 'desc')
    //.startAfter() must use doc()
    .limit(100)
  return query
}

function _buildItemFromSnapshot(snapshot) {
  const row = snapshot.data()
  return {
    id: snapshot.id,
    ...row,
    //date: moment(row.date.toDate()),
    schedule: {
      ...row.schedule,
      date: moment(row.schedule.date.toDate()),
    },
  }
}

function _buildListFromSnapshot(snapshot) {
  return snapshot.docs.map(_buildItemFromSnapshot)
}

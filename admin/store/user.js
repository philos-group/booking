import { auth } from 'firebase/app'

export const state = () => ({
  isAuthenticated: false,
  unsbubscribe: null,
  profile: {},
})

export const getters = {}

export const mutations = {
  SET_USER(state, user) {
    state.profile = user
    state.isAuthenticated = true
  },
  REMOVE_USER(state) {
    state.profile = null
    state.isAuthenticated = false
  },
  SET_UNSUBSCRIBE(state, unsbscribe) {
    state.unsbubscribe = unsbscribe
  },
}

export const actions = {
  startListeningToUserChanges(ctx) {
    const unsbubscribe = auth().onAuthStateChanged((user) => {
      if (user) {
        ctx.commit('SET_USER', {
          displayName: user.displayName,
          email: user.email,
          photoURL: user.photoURL,
          isAnonymous: user.isAnonymous,
          uid: user.uid,
        })
      } else {
        ctx.commit('REMOVE_USER')
      }
    })
    ctx.commit('SET_UNSUBSCRIBE', unsbubscribe)
  },
  stopListeningToUserChanges({ commit, state }) {
    if (state.unsbubscribe) state.unsbubscribe()
    commit('SET_UNSUBSCRIBE', null)
  },
  async signOut() {
    await auth().signOut()
    window.location.reload()
  },
}

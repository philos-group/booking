# Web 

Anonymouse Users => move to special loggedin User

# Admin

**Authentication** : Only Google Authentication

_Frontend_

- nuxt plugin Auto Login
- if : it is not back from Authentication call
  - if user is not logged in already
    > login using google
  - if user is logged
    > check if it has custom claim "Group" : "admins" 
    > if claim not exists, check creationDate and lastLogin date are equal  aka user first login. the reAuthenticate  { Fix issue : PHB-1 first login login must logout and login again }
    > else render 403 error.

_Backend_

- cloud function
- listener when new user is created.
- search for it in invitations list.
  - if exists
    > create a custom claim "Group" : "admins" & update user collection (IsAccepted = true, MemberSince = creation date)
  - if not exists
    > delete the user

**Invitations**
Collections : Users
|Name|Type|Description|
| ---|:--:| ---------|
| Id | string | Id in Auth table
| displayName | string | from provider
| profilePicture| Url | from provider
| accepted | Boolean | 'accepted' : if logged in before / n/a : never logged in
| memberSince | Timestamp | creatation date

---

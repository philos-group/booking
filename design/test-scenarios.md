# Test Cases

### Scenario 1: Create Event
__Description__:
 Create Event in admin, it should be displayed in admin and on the app.  

__Steps__
- Login to Admin Panel
- Go to Canlendar  
- Click on Create new 
- Fill in Event Information
- Save 

__Expected__
- search for the event specific date 
- Event should be displayed
- go to the app
- fill in previous steps (if exists)
- navigate for the month of the created event, event should be displayed with correct values.

### Scenario 2: App Request  
__Description__:
 Create request from the app, should have a QR code and send an email. and should be displayed in event requests and in event printout.  

__Steps__
- from app, fill in the wizard steps and click submit

__Expected__
- from admin, search for selected event
- click on the event location 
- check the requests page, request should be displayed
- QR should exists
- check that email must be recieved
- number of availale seats must be less with the number of attendees in the request

### Scenario 3: create admin request 
__Description__:
 Create request from the admin, should have a QR code but no email. and should be displayed in event requests and in event printout.  

__Steps__
- from admin, go the selected event
- click on admin request
- fill in the form.

__Expected__
- from admin, search for selected event
- click on the event location 
- check the requests page, request should be displayed
- QR should exists
- number of availale seats must be less with the number of attendees in the request


### Scenario 4: Delete Events with requests
__Description__:
 Delete event should delete its event and all requested related to it. 

__Steps__
- Login to Admin Panel
- Go to Canlendar  
- select a specific event 
- make sure it has some requests  
- click delete event and in the confirmation popup click yes

__Expected__
- search for the event specific date 
- Event should not be displayed
- go to the app
- fill in previous steps (if exists)
- navigate for the month of the created event, event should be not be displayed 


## Future 
##### Scenario 5: Admin Invitations
##### Scenario 6: Change SMTP settings
##### Scenario 7: Wizard Configuration
##### Scenario 8: Integration Configuration
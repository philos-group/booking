import { Request, Response } from "firebase-functions";
import { each } from "lodash";
import * as ProcessManager from "../infrastructure/process";

export default async function (req: Request, res: Response) {
  const stages = await ProcessManager.deadMessages("registration");
  each(stages, (messages) =>
    each(messages, (msg) => (msg.date = msg.timestamp?.toDate()))
  );
  res.status(200).json(stages);
}

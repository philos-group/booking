import * as admin from "firebase-admin";
import * as functions from "firebase-functions";
import express from "express";
import cors from "cors";

import deadMessages from "./dead-messages";
import retry from "./retry";

const app = express();

const authenticate = async (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => {
  if (
    !req.headers.authorization ||
    !req.headers.authorization.startsWith("Bearer ")
  ) {
    res.status(403).send("Unauthorized");
    return;
  }
  const idToken = req.headers.authorization.split("Bearer ")[1];
  try {
    const decodedIdToken = await admin.auth().verifyIdToken(idToken);
    if (decodedIdToken["Group"] === "admins") {
      req.user = decodedIdToken;
      next();
      return;
    } else {
      res.status(403).send("Unauthorized");
      return;
    }
  } catch (e) {
    res.status(403).send("Unauthorized");
    return;
  }
};

app.use(cors());
app.use(authenticate);

app.get("/process/dead-messages", deadMessages);
app.patch("/process/retry", retry);

export const api = functions.https.onRequest(app);

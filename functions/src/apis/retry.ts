import { Request, Response } from "firebase-functions";
import * as ProcessManager from "../infrastructure/process";

export default async function (req: Request, res: Response) {
  await ProcessManager.retry("registration");
  res.sendStatus(200);
}

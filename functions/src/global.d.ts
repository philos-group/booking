//import * as admin from "firebase-admin";

declare module "*.txt";
declare module "*.html";

declare namespace Express {
  export interface Request {
    user?: Object;
  }
}

import { initializeApp } from "firebase-admin";

initializeApp();

export { registrationListeners } from "./process/registration";
export { onNewRequest, onNewUser } from "./listeners";
export { api } from "./apis";

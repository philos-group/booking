import * as processManager from "./processes";
import { admin, Message } from "../service-bus";

export interface DeadMessages {
  [step: string]: Message[];
}

export default async function deadMessages(
  name: string
): Promise<DeadMessages> {
  const process = processManager.get(name);
  const deadMessages: DeadMessages = {};
  for (const step of process.steps) {
    const topic = await admin.serviceBus().topic(step.topicName || step.name);
    deadMessages[step.name] = await topic.readAllMessages();
  }
  return deadMessages;
}

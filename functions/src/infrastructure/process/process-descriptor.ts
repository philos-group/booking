export interface ProcessDescriptor {
  name: string;
  steps: {
    name: string;
    function: Function;
    topicName?: string;
  }[];
}

export enum StepStatus {
  NotStarted = 0,
  InProgress = 1,
  Done = 2,
  Error = 3,
}

export interface AttributeSteps {
  name: string;
  topic: string;
  status: StepStatus;
  error?: Object;
}

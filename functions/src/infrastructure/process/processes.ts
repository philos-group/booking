import { each } from "lodash";

import { ProcessDescriptor } from "./process-descriptor";

const processes: {
  [name: string]: ProcessDescriptor;
} = {};

export function get(name: string) {
  return processes[name];
}

export function set(descriptor: ProcessDescriptor) {
  each(descriptor.steps, step =>  step.topicName = `${descriptor.name}_${step.name}`);
  processes[descriptor.name] = descriptor;
}
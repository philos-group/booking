import { CloudFunction, Change } from "firebase-functions";
import { QueryDocumentSnapshot } from "firebase-functions/lib/providers/firestore";
import { each, extend } from "lodash";

import { admin, functions, Message } from "../service-bus";
import {
  ProcessDescriptor,
  AttributeSteps,
  StepStatus,
} from "./process-descriptor";
import * as processManager from "./processes";
import { MessageContext } from "../service-bus/function-extension";

export default function registerProcess(
  process: ProcessDescriptor
): {
  [name: string]:
    | CloudFunction<QueryDocumentSnapshot>
    | CloudFunction<Change<QueryDocumentSnapshot>>;
} {
  processManager.set(process);
  const serviceBus = admin.serviceBus();
  const exportedListners: {
    [name: string]:
      | CloudFunction<QueryDocumentSnapshot>
      | CloudFunction<Change<QueryDocumentSnapshot>>;
  } = {};

  const listenerFnc = (ndx: number, handler: Function) => {
    return async (msg: Message, ctx: MessageContext) => {
      const input = msg.body;
      const steps = msg.attributes?.steps as AttributeSteps[];

      steps[ndx].status = StepStatus.InProgress;
      await ctx.updateMessage(msg);

      try {
        let output = await handler.apply(msg, [input]);
        msg.body = extend(output, input);
        steps[ndx].status = StepStatus.Done;
        await ctx.deleteMessage();
        if (ndx + 1 < steps.length) {
          let nextTopic = await serviceBus.topic(steps[ndx + 1].topic);
          await nextTopic.publishMessage(msg);
        }
      } catch (err) {
        steps[ndx].status = StepStatus.Error;
        steps[ndx].error = err.toString();
        await ctx.updateMessage(msg);
      }
    };
  };

  each(process.steps, (step, ndx) => {
    exportedListners[process.name + "_" + step.name] = functions
      .topic(step.topicName || step.name)
      .onMessage(listenerFnc(ndx, step.function), { readMode: "pickLock" });
  });
  return exportedListners;
}

import * as processManager from "./processes";
import { admin } from "../service-bus";

export default async function retry(name: string): Promise<void> {
  const process = processManager.get(name);
  for (const step of process.steps) {
    const topic = await admin.serviceBus().topic(step.topicName || step.name);
    const msgs = await topic.readAllMessages();
    for (const msg of msgs) {
      await topic.retryMessage(msg);
    }
  }
}

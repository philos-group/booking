import { firestore } from "firebase-admin";

import { Message } from "./message";

let sb: ServiceBus;

export class ServiceBus {
  private firestoreContext_: firestore.Firestore;
  private serviceBusCollection_: firestore.CollectionReference;
  private topicDoc_: firestore.DocumentReference;

  constructor() {
    this.firestoreContext_ = firestore();
    this.serviceBusCollection_ = this.firestoreContext_.collection(
      process.env.C_SERVICEBUSS_COLL as string
    );
    this.topicDoc_ = this.serviceBusCollection_.doc(
      process.env.C_TOPIC_DOC as string
    );
    this.topicDoc_.set({ lastConnection: new Date() });
  }

  async topics(): Promise<ServiceBusTopic[]> {
    const topicsColl = await this.topicDoc_.listCollections();
    return topicsColl.map(
      (t) => new ServiceBusTopic(t as firestore.CollectionReference<Message>)
    );
  }

  async topic(name: string): Promise<ServiceBusTopic> {
    const topicColl = this.topicDoc_.collection(name);
    return new ServiceBusTopic(
      topicColl as firestore.CollectionReference<Message>
    );
  }
  async createTopic(name: string): Promise<ServiceBusTopic> {
    const topicColl = this.topicDoc_.collection(name);
    return new ServiceBusTopic(
      topicColl as firestore.CollectionReference<Message>
    );
  }
  async removeTopic(name: string): Promise<void> {
    const topicColl = this.topicDoc_.collection(name);
    const allMessages = await topicColl.listDocuments();
    for (const msg of allMessages) await msg.delete();
  }
}

export class ServiceBusTopic {
  constructor(private topicColl: firestore.CollectionReference<Message>) {}
  async publishMessage(message: Message): Promise<string> {
    message.timestamp = firestore.Timestamp.now();
    const doc = this.topicColl.doc();
    message.id = doc.id;
    if (message.retry) delete message.retry;
    await doc.set(message);
    return message.id;
  }
  async retryMessage(message: Message): Promise<string> {
    message.timestamp = firestore.Timestamp.now();
    const oldDoc = this.topicColl.doc(message.id || "");
    await oldDoc.delete();
    const newDoc = this.topicColl.doc(message.id || "");
    message.retry = true;
    newDoc.set(message);
    return newDoc.id;
  }

  async readAllMessages(): Promise<Message[]> {
    return (await this.topicColl.get()).docs.map((doc) => ({id : doc.id, ...doc.data()}));
  }

  // Not Used
  async receiveAndDeleteMessage(): Promise<Message | null> {
    const result = await this.topicColl
      .orderBy("timestamp", "desc")
      .limit(1)
      .get();

    if (result.empty) return null;

    const doc = result.docs[0].ref;
    const message = result.docs[0].data();

    if (message.lock) {
      return null;
    }
    const data = {
      ...message,
      id: doc.id,
    };
    await doc.delete();
    return data;
  }
  async pickLockMessage(): Promise<Message | null> {
    const result = await this.topicColl
      .orderBy("timestamp", "desc")
      .limit(1)
      .get();

    if (result.empty) return null;

    const doc = result.docs[0].ref;
    const message = result.docs[0].data();

    if (message.lock) {
      return null;
    }
    message.lock = new Date().getTime().toString();
    await doc.update(message);

    return {
      ...message,
      id: doc.id,
    };
  }
  async unlockMessage(key: string): Promise<void> {
    const result = await this.topicColl.where("lock", "==", key).limit(1).get();

    if (result.empty) throw new Error(`${key} dosent exist`);

    const doc = result.docs[0].ref;
    await doc.update({
      lock: firestore.FieldValue.delete(),
    });
  }
  async deleteMessage(): Promise<void> {
    const result = await this.topicColl
      .orderBy("timestamp", "desc")
      .limit(1)
      .get();

    if (result.empty) return;

    const doc = result.docs[0].ref;
    const message = result.docs[0].data();

    if (message.lock) {
      if (result.empty) throw new Error(`message locked`);
    }
    await doc.delete();
  }
}

export function serviceBus() {
  if (!sb) sb = new ServiceBus();
  return sb;
}

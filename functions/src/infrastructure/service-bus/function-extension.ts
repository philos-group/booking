import { CloudFunction, firestore, EventContext } from "firebase-functions";
import { QueryDocumentSnapshot } from "firebase-functions/lib/providers/firestore";
import { firestore as adminFirestore } from "firebase-admin";
import { extend } from "lodash";

import { Message } from "./message";

export function topic(name: string): TopicBuilder {
  return new TopicBuilder(name);
}

export interface OnMessageOptions {
  readMode: "receiveAndDelete" | "pickLock";
}
export interface MessageContext {
  auth?: {
    token: object;
    uid: string;
  };
  eventId: string;
  timestamp: string;
  readMode?: "receiveAndDelete" | "pickLock";
  deleteMessage: () => Promise<void>;
  updateMessage: (msg: any) => Promise<void>;
  lockMessage: () => Promise<void>;
  unlockMessge: () => Promise<void>;
}

export class TopicBuilder {
  constructor(private name: string) {}

  onMessage(
    handler: (
      message: Message,
      context: MessageContext
    ) => PromiseLike<any> | any,
    options?: OnMessageOptions
  ): CloudFunction<QueryDocumentSnapshot> {
    options = extend({ readMode: "receiveAndDelete" }, options);
    return firestore
      .document(
        `${process.env.C_SERVICEBUSS_COLL}/${process.env.C_TOPIC_DOC}/${this.name}/{message}`
      )
      .onCreate(async (doc, ctx) => {
        const sbContext = this._buildContext(ctx, doc);
        await this._executeHandler(
          handler,
          doc.data() as Message,
          sbContext,
          options
        );
      });
  }

  private _buildContext(
    ctx: EventContext,
    doc: firestore.DocumentSnapshot
  ): MessageContext {
    return {
      auth: ctx.auth,
      eventId: ctx.eventId,
      timestamp: ctx.timestamp,
      updateMessage: async (msg: Message) => {
        await doc.ref.update(msg);
      },
      deleteMessage: async () => {
        await doc.ref.delete();
      },
      lockMessage: async () => {
        await doc.ref.set(
          {
            lock: new Date().getTime().toString(),
            retry: adminFirestore.FieldValue.delete(),
          },
          { merge: true }
        );
      },
      unlockMessge: async () => {
        await doc.ref.set(
          {
            lock: adminFirestore.FieldValue.delete(),
            retry: adminFirestore.FieldValue.delete(),
          },
          { merge: true }
        );
      },
    };
  }

  private async _executeHandler(
    handler: Function,
    message: Message,
    ctx: MessageContext,
    options: OnMessageOptions = { readMode: "receiveAndDelete" }
  ) {

    if (options?.readMode == "pickLock") {
      await ctx.lockMessage();
    }
    
    await handler(message, ctx);
    
    if (options?.readMode == "receiveAndDelete") {
      await ctx.deleteMessage();
    }
    
  }
}

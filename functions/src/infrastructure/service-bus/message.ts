import { firestore } from "firebase-admin";

export interface Message {
  body: any;
  id?: string;
  timestamp?: firestore.Timestamp;
  date?: Date;
  Label?: string;
  attributes?: { [key: string]: any };
  lock?: string;
  retry?: boolean;
}

import { firestore } from "firebase-functions";
import { firestore as adminFirestore } from "firebase-admin";

import { start } from "../infrastructure/process";

export const onNewRequest = firestore
  .document("/requests/{requestId}")
  .onCreate(async (doc) => {
    const scheduleId = doc.data().schedule.id;

    const schedule = await adminFirestore().doc(`schedule/${scheduleId}`).get();
    await schedule.ref.update({
      countLeft: schedule.data()?.countLeft - doc.data().numberOfAttendees,
    });

    return await start("registration", {
      code: doc.id,
      path: doc.ref.path,
    });
  });

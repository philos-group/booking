import { auth } from "firebase-functions";
import { firestore, auth as adminAuth } from "firebase-admin";

export const onNewUser = auth.user().onCreate(async (user) => {
  const invitations = await firestore()
    .collection("invitations")
    .where("email", "==", user.email)
    .limit(1)
    .get();

  if (invitations.size > 0) {
    const invitation = invitations.docs[0];
    await invitation.ref.set({ status: "accepted" }, { merge: true });
    await adminAuth().setCustomUserClaims(user.uid, {
      Group: "admins",
    });
  }
});

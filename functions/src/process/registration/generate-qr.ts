import { firestore, storage } from "firebase-admin";
import QRCode from "qrcode";

export default async function generateQR(params: { path: string }) {
  const doc = await firestore().doc(params.path);
  const row = (await doc.get()).data() as any;
  // =========================================
  const file = storage().bucket().file(`qr/${doc.id}.png`);

  await new Promise(async (res, rej) => {
    const fs = file.createWriteStream({
      resumable: false,
      contentType: "	image/png",
    });
    fs.on("finish", () => res());
    fs.on("error", (err) => rej(err));
    try {
      const dataStr = generateQrString(row);
      await QRCode.toFileStream(fs, dataStr);
    } catch (err) {
      rej(err);
    }
  });

  await file.makePublic();

  const url = await file.getSignedUrl({
    action: "read",
    expires: "03-09-2491",
  });

  await doc.set({ qrCode: url[0] }, { merge: true });
}

function generateQrString(row: any) {
  let str = `Name:${row.firstName}\nAttendees:${
    row.numberOfAttendees
  }\nLocation:${
    row.schedule.location
  }\nDate:${row.schedule.date.toDate().toDateString()}\n`;
  if (row.createdBy) {
    str += `Admin:${row.createdBy}\nComments:${row.comments || ""}`;
  }
  return str;
}

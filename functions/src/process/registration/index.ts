import * as processManager from "../../infrastructure/process";

import generateQRFunction from "./generate-qr";
import sendEmailFunction from "./send-mail";

export const registrationListeners = processManager.register({
  name: "registration",
  steps: [
    {
      name: "GenerateQR",
      function: generateQRFunction,
    },
    {
      name: "SendEmail",
      function: sendEmailFunction,
    },
  ],
});

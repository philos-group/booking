import nodemailer from "nodemailer";
import { firestore, remoteConfig } from "firebase-admin";
import { template } from "lodash";
import moment from "moment";

import emailTemplate from "./email-template.html";

const compiledEmail = template(emailTemplate);

export default async function sendMail(params: { path: string }) {
  const ref = await firestore().doc(params.path).get();
  const docData = ref.data() || {};

  if (docData.email == "admin-reserved") {
    return;
  }

  const cfg = await remoteConfig().getTemplate();

  const transport = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: _getValueFromConfig(cfg, "MAIL_USER"),
      pass: _getValueFromConfig(cfg, "MAIL_PASS"),
    },
  });

  docData.schedule.date = moment(docData?.schedule.date.toDate());

  await transport.sendMail({
    from: _getValueFromConfig(cfg, "MAIL_USER"),
    to: docData?.email || "default@philos-platform.com",
    subject: _getValueFromConfig(cfg, "MAIL_SUBJECT"),
    html: compiledEmail({ id: ref.id, ...docData }),
  });
}

function _getValueFromConfig(
  cfg: remoteConfig.RemoteConfigTemplate,
  key: string,
  defaultValue: string = ""
) {
  const val = cfg.parameters[key]
    .defaultValue as remoteConfig.ExplicitParameterValue;
  return val.value || defaultValue;
}

"use strict";

const nodeExternals = require("webpack-node-externals");
const dotenv = require("dotenv-webpack");
const webpack = require("webpack");
const _ = require("lodash");

const result = {
  ...process.env
};

module.exports = {
  mode: "development",
  devtool: "inline-source-map",
  output: {
    path: __dirname,
    filename: "./index.js",
    libraryTarget: "this",
  },
  target: "node",
  module: {
    rules: [
      {
        test: /\.ts?$/,
        loader: "ts-loader",
      },
      {
        test: /\.html$/i,
        use: "raw-loader",
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"],
  },
  plugins: [new dotenv(), new webpack.EnvironmentPlugin(result)],
  externals: [nodeExternals()],
  node: {
    console: false,
    global: false,
    process: false,
    Buffer: false,
    __filename: false,
    __dirname: false,
  },
};

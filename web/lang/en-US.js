export default {
  WIZARD_INFO_TITLE: 'Book Liturgy',
  WIZARD_INFO_SLUG: `Each member or family must book. We remind you to correctly write the email in order to receive the booking confirmation. To
   cancel the reservation`,
  WIZARD_INFO_ClickHere: 'Click Here',
  WIZARD_INFO_NEXT: 'Next',
  WIZARD_INFO_BACK: 'Previous',
  WIZARD_INFO_FINISH: 'Submit',
  //=================================================
  WIZARD_INFO_DATE_TITLE: 'Date',
  WIZARD_INFO_DATE_HEADING:
    'Please Select the liturgy you are planning to attend',
  WIZARD_INFO_DATE_NODATA: 'No liturgy on this period',
  WIZARD_INFO_DATE_COUNTLEFT: 'seats left',
  WIZARD_INFO_DATE_TOATCOUNT: 'total seats',
  WIZARD_INFO_DATE_BOOKEDCOUNT: 'booked seats',
  WIZARD_INFO_DATE_ERROR: 'Please Select the liturgy you want to attend.',
  //=================================================
  WIZARD_INFO_INFO_TITLE: 'Information',
  WIZARD_INFO_INFO_HEADING: 'Please fill in the attendant persons information',
  WIZRD_INFO_INFO_FIRSTNAME: 'First Name',
  WIZRD_INFO_INFO_LASTNAME: 'Last Name',
  WIZRD_INFO_INFO_NOATTENDEES: 'Number Of Attendees',
  WIZRD_INFO_INFO_EMAIL: 'Email',
  WIZRD_INFO_INFO_MOBILE: 'Mobile',
  WIZARD_INFO_INFO_REQUIRED: 'required',
  WIZARD_INFO_INFO_ERR_REQUIRED: 'this field is required',
  WIZARD_INFO_INFO_ERR_LENGTH: 'maximum length is 50 characters',
  WIZARD_INFO_INFO_ERR_NUM: 'only numbers allowed',
  WIZARD_INFO_INFO_ERR_COUNT: 'only {countLeft} seats left',
  WIZARD_INFO_INFO_ERR_EMAIL: 'inavlid email format',
  WIZARD_INFO_INFO_ERR_MOBILE: 'Mobile number must be 11 digits',
  //==================================================
  WIZARD_INFO_REVIEW_TITLE: 'Review',
  WIZARD_INFO_REVIEW_HEADING: 'Please review before submit your application',
  WIZRD_INFO_REVIEW_NAME: 'Name',
  WIZRD_INFO_REVIEW_NOATTENDEES: 'Number Of Attendees',
  WIZRD_INFO_REVIEW_EMAIL: 'Email',
  WIZRD_INFO_REVIEW_MOBILE: 'Mobile',
  WIZRD_INFO_REVIEW_DATE: 'Date & time',
  WIZRD_INFO_REVIEW_LOCATION: 'Location'
}

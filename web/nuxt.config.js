export default {
  mode: 'spa',
  target: 'static',
  /*
   ** Headers of the page
   */
  head: {
    title: 'Book Liturgy',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
      {
        rel: 'stylesheet',
        href:
          'https://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Muli:400,300'
      },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css2?family=Tajawal:wght@700&display=swap'
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#F3BB45',
    continuous: true,
    height: '15px'
  },
  /*
   ** Global CSS
   */
  css: ['@/assets/css/themify-icons.css', '@/assets/scss/theme.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/firebase.js',
    '~/plugins/vue-date-pick.js',
    '~/plugins/vuelidate.js',
    '~/plugins/local-moment.js'
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    'vue-scrollto/nuxt',
    'nuxt-i18n'
  ],
  i18n: {
    locales: [
      {
        code: 'en',
        file: 'en-US.js',
        iso: 'en-US'
      },
      {
        code: 'ar',
        file: 'ar-EG.js',
        iso: 'ar-EG'
      }
    ],
    lazy: true,
    langDir: 'lang/',
    seo: true,
    defaultLocale: 'en'
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  }
}

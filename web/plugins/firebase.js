/* eslint-disable no-undef */
import { initializeApp } from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

export default async function() {
  const response = await fetch('/__/firebase/init.json')
  const config = await response.json()
  initializeApp(config)
  // if (location.hostname === 'localhost') {
  //   firestore().settings({
  //     host: 'localhost:8080',
  //     ssl: false
  //   })
  // }
}

import moment from 'moment'
import 'moment/locale/ar-sa'
import 'moment/locale/en-gb'

export default function({ app, store }) {
  moment.locale(app.i18n.locale)
  app.i18n.beforeLanguageSwitch = (oldLocale, newLocale) => {
    moment.locale(newLocale)
    store.dispatch('lookups/swicthLang', { locale: newLocale })
  }
}

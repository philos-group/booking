import { firestore } from 'firebase/app'

export const state = () => ({
  data: {},
  confirmation: null,
  wizardInfo: {
    title: 'WIZARD_INFO_TITLE',
    slug: 'WIZARD_INFO_SLUG',
    currentStep: 0,
    steps: [
      {
        title: 'WIZARD_INFO_DATE_TITLE',
        icon: 'ti-time',
        heading: 'WIZARD_INFO_DATE_HEADING',
        checked: false
      },
      {
        title: 'WIZARD_INFO_INFO_TITLE',
        icon: 'ti-user',
        heading: 'WIZARD_INFO_INFO_HEADING',
        checked: false
      },
      {
        title: 'WIZARD_INFO_REVIEW_TITLE',
        icon: 'ti-eye',
        heading: 'WIZARD_INFO_REVIEW_HEADING',
        checked: false
      }
    ]
  }
})

export const mutations = {
  changeStep(state, { data, newStep }) {
    let checkedStep = state.wizardInfo.steps[state.wizardInfo.currentStep]
    state.data = {
      ...state.data,
      ...data
    }
    checkedStep.checked = true
    state.wizardInfo.currentStep = newStep
  },
  submiting() {},
  submitted(state, { confirmation }) {
    let checkedStep = state.wizardInfo.steps[state.wizardInfo.currentStep]
    checkedStep.checked = true
    state.wizardInfo.currentStep = null
    state.confirmation = confirmation
  }
}

export const actions = {
  changeStep({ commit }, payload) {
    commit('changeStep', payload)
  },
  async submit({ commit, state }) {
    window.$nuxt.$root.$loading.start()
    commit('submiting')
    const fs = firestore()

    const model = state.data
    const scheduleRef = fs.doc(`schedule/${model.scheduleItem.id}`)
    const newReqRef = fs.collection('requests').doc()

    await fs.runTransaction(async transaction => {
      const scheduleItem = await transaction.get(scheduleRef)

      if (scheduleItem.data().countLeft < model.numberOfAttendees)
        return Promise.reject(
          'Oops!! someone just booked the seates left, please start from the begining'
        )

      transaction.set(newReqRef, {
        email: model.email,
        firstName: model.firstName,
        lastName: model.lastName,
        mobile: model.mobile,
        numberOfAttendees: model.numberOfAttendees,
        schedule: {
          date: model.scheduleItem.date.toDate(),
          id: model.scheduleItem.id,
          location: model.scheduleItem.location
        }
      })
    })

    commit('submitted', { confirmation: newReqRef.id })
    window.$nuxt.$root.$loading.finish()
  }
}

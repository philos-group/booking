import { each } from 'lodash'
import moment from 'moment'

export const state = () => ({
  schedule: []
})

export const mutations = {
  changeSchedule(state, schedule) {
    state.schedule = schedule
  },
  changeLanguage(state, { locale }) {
    each(state.schedule, item => {
      item.date.locale(locale)
    })
  }
}

export const actions = {
  changeSchedule({ commit }, schedule) {
    each(schedule, itm => {
      itm.date = moment(itm.date.toDate())
    })
    schedule = _.orderBy(schedule, 'date')
    commit('changeSchedule', schedule)
  },
  swicthLang({ commit }, payload) {
    commit('changeLanguage', payload)
  }
}

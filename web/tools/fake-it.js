const faker = require('faker')
const fs = require('fs')
const moment = require("moment");

function generateLookups() {
  locations = ["St Mina - Södertälje",
   "St Mary - Midsummer", 
   "St Markus - Norsborg", 
   "St Paul - Tensta"];

  var data = {
    schedule : []
  }
  var i = 50
  while (i > 0) {
    var count = faker.random.number(50);
    var left = faker.random.number(count);
    data.schedule.push({
        id : faker.internet.password(),
        location : faker.random.arrayElement(locations),
        date : moment(faker.date.future(0.5)),
        countLeft : left,
        count : count,
    })
    i--
  }
  fs.writeFileSync(
    __dirname + '/../store/data/lookups.json',
    JSON.stringify(data, null, 1)
  )
}

generateLookups()
